package com.simonall.constants;

/**
 * SQL关键字
 * 
 * @author simon
 */
public class SQL {

	public static final String SELECT = " SELECT ";

	public static final String UPDATE = " UPDATE ";

	public static final String DELETE = " DELETE ";

	public static final String INSERT = " INSERT ";

	public static final String FROM = " FROM ";

	public static final String ABSOLUTE = " ABSOLUTE ";

	public static final String AND = " AND ";

	public static final String ADD = " ADD ";

	public static final String ADDDATE = " ADDDATE ";

	public static final String AFTER = " AFTER ";

	public static final String ALTER = " ALTER ";

	public static final String ALL = " ALL ";

	public static final String ASC = " ASC ";

	public static final String AS = " AS ";

	public static final String ANY = " ANY ";

	public static final String BEFORE = " BEFORE ";

	public static final String BEGIN = " BEGIN ";

	public static final String BETWEEN = " BETWEEN ";

	public static final String BOTH = " BOTH ";

	public static final String BY = " BY ";

	public static final String BIT = " BIT ";

	public static final String BOOLEAN = " BOOLEAN ";

	public static final String CHAR = " CHAR ";

	public static final String CHARACTER = " CHARACTER ";

	public static final String CHECK = " CHECK ";

	public static final String CLASS = " CLASS ";

	public static final String COMMIT = " COMMIT ";

	public static final String CONNECT = " CONNECT ";

	public static final String CONSTRAINT = " CONSTRAINT ";

	public static final String CONTINUE = " CONTINUE ";

	public static final String COUNT = " COUNT ";

	public static final String CREATE = " CREATE ";

	public static final String CREATEDB = " CREATEDB ";

	public static final String CREATEUSER = " CREATEUSER ";

	public static final String CROSS = " CROSS ";

	public static final String CURRENT = " CURRENT ";

	public static final String CURRENT_PATH = " CURRENT_PATH ";

	public static final String CURRENT_ROLE = " CURRENT_ROLE ";

	public static final String CURRENT_DATE = " CURRENT_DATE ";

	public static final String CURRENT_TIME = " CURRENT_TIME ";

	public static final String CURRENT_TIMESTAMP = " CURRENT_TIMESTAMP ";

	public static final String CURRENT_USER = " CURRENT_USER ";

	public static final String DAY = " DAY ";

	public static final String DATE = " DATE ";

	public static final String DEC = " DEC ";

	public static final String DECIMAL = " DECIMAL ";

	public static final String DECLARE = " DECLARE ";

	public static final String DEFAULT = " DEFAULT ";

	public static final String DESC = " DESC ";

	public static final String DESTROY = " DESTROY ";

	public static final String DISTINCT = " DISTINCT ";

	public static final String DO = " DO ";

	public static final String DOUBLE = " DOUBLE ";

	public static final String DROP = " DROP ";

	public static final String EACH = " EACH ";

	public static final String ELSE = " ELSE ";

	public static final String END = " END ";

	public static final String EQUALS = " EQUALS ";

	public static final String EVERY = " EVERY ";

	public static final String EXCEPT = " EXCEPT ";

	public static final String EXEC = " EXEC ";

	public static final String EXECUTE = " EXECUTE ";

	public static final String EXISTS = " EXISTS ";

	public static final String FOR = " FOR ";

	public static final String FUNCTION = " FUNCTION ";

	public static final String FALSE = " FALSE ";

	public static final String FLOAT = " FLOAT ";

	public static final String GLOBAL = " GLOBAL ";

	public static final String GOTO = " GOTO ";

	public static final String GROUP = " GROUP ";

	public static final String HAVING = " HAVING ";

	public static final String HOUR = " HOUR ";

	public static final String IDENTITY = " IDENTITY ";

	public static final String IN = " IN ";

	public static final String INCREMENT = " INCREMENT ";

	public static final String INT = " INT ";

	public static final String INTEGER = " INTEGER ";

	public static final String INTO = " INTO ";

	public static final String IS = " IS ";

	public static final String JOIN = " JOIN ";

	public static final String LAST = " LAST ";

	public static final String LEFT = " LEFT ";

	public static final String LENGTH = " LENGTH ";

	public static final String LESS = " LESS ";

	public static final String LIKE = " LIKE ";

	public static final String LIMIT = " LIMIT ";

	public static final String LOWER = " LOWER ";

	public static final String MAX = " MAX ";

	public static final String MIN = " MIN ";

	public static final String MINUTE = " MINUTE ";

	public static final String MODE = " MODE ";

	public static final String NOT = " NOT ";

	public static final String NULL = " NULL ";

	public static final String NUMBER = " NUMBER ";

	public static final String NUMERIC = " NUMERIC ";

	public static final String OF = " OF ";

	public static final String ON = " ON ";

	public static final String ONLY = " ONLY ";

	public static final String OR = " OR ";

	public static final String ORDER = " ORDER ";

	public static final String PRIMARY = " PRIMARY ";

	public static final String RETURN = " RETURN ";

	public static final String RIGHT = " RIGHT ";

	public static final String ROLLBACK = " ROLLBACK ";

	public static final String SELF = " SELF ";

	public static final String SEQUENCE = " SEQUENCE ";

	public static final String SET = " SET ";

	public static final String SUM = " SUM ";

	public static final String TRUE = " TRUE ";

	public static final String TABLE = " TABLE ";

	public static final String THEN = " THEN ";

	public static final String THAN = " THAN ";

	public static final String TIME = " TIME ";

	public static final String TIMESTAMP = " TIMESTAMP ";

	public static final String TIMEZONE_HOUR = " TIMEZONE_HOUR ";

	public static final String TIMEZONE_MINUTE = " TIMEZONE_MINUTE ";

	public static final String TO = " TO ";

	public static final String TREAT = " TREAT ";

	public static final String TRIM = " TRIM ";

	public static final String UNION = " UNION ";

	public static final String UNIQUE = " UNIQUE ";

	public static final String UPPER = " UPPER ";

	public static final String USING = " USING ";

	public static final String VARCHAR = " VARCHAR ";

	public static final String VERSION = " VERSION ";

	public static final String WHEN = " WHEN ";

	public static final String WHERE = " WHERE ";

	public static final String WITH = " WITH ";

	public static final String WITHOUT = " WITHOUT ";

	public static final String WHENEVER = " WHENEVER ";

	public static final String YEAR = " YEAR ";

	public static final String ZONE = " ZONE ";
}
