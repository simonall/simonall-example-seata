package com.simonall.utils.order;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Order Initialize
 * 
 * @author simonall
 */
@Component
@Order(value = Integer.MIN_VALUE)
public class OrderInitialize implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		OrderSingleton.getInstance().initialize();
	}

}
