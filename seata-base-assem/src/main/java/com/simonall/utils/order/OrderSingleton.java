package com.simonall.utils.order;

import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.simonall.utils.date.BaseDate;
import com.simonall.utils.date.convert.config.DateFormatConfig;
import com.simonall.utils.date.convert.motion.DateConvertStringMotion;

/**
 * Order Singleton
 * 
 * @author simon
 */
public final class OrderSingleton {

	private static OrderSingleton instance = new OrderSingleton();

	public static OrderSingleton getInstance() {
		return instance;
	}

	private static ExecutorService fixedPool = Executors.newFixedThreadPool(1);

	// Init
	public void initialize() throws RuntimeException {
		fixedPool.execute(new CreateOrderNumImpl());
	}

	// 定义存放订单流水号单例
	private BlockingQueue<String> values = new ArrayBlockingQueue<String>(1000, true);

	// 获取订单流水号
	private final String getValue() {
		try {
			return values.take();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}

	// 生成订单流水号
	private final void setValue(String value) throws InterruptedException {
		this.values.put(value);
	}

	// 获取今天的日期
	DateConvertStringMotion motion = new DateConvertStringMotion();

	private final String getToDayYYYYMMDD() {
		return motion.convert(DateFormatConfig.YYYYMMDDL(), BaseDate.getNowTime());
	}

	// -----------------------------------下面是辛勤的生成者-----------------------------------------

	/**
	 * 订单流水号生产者
	 * 
	 * @author simon
	 */
	private final class CreateOrderNumImpl implements Runnable {

		// 随机数
		private Random ran = new Random();

		@Override
		public void run() {
			try {
				do {
					setValue(createOrderNumber());
				} while (true);
			} catch (InterruptedException e) {
				// 1.写出线程等待异常
				e.printStackTrace();

				// 2.释放当前独立线程
				Thread.currentThread().interrupt();

				// 3.重新创建流水号生产者线程
				fixedPool.execute(new CreateOrderNumImpl());
			}
		}

		// 随机流水号（线程堵塞）
		private String createOrderNumber() throws InterruptedException {
			synchronized (this) {
				Thread.sleep(1);
				String current = String.valueOf(System.currentTimeMillis());
				String add = new String();
				do {
					Integer random = ran.nextInt(10);
					add += String.valueOf(random);
				} while (add.length() < 5);
				return current.substring(6, current.length()) + add;
			}
		}
	}

	// ----------------------------------------------------下面是对外提供的方法----------------------------------------------------------
	/**
	 * 流水号
	 * 
	 * @return String
	 */
	public String getOrderNum() {

		return getToDayYYYYMMDD() + getValue();
	}

	/**
	 * 生成动态数字码【6位数字验证码】
	 * 
	 * @return String
	 */
	public String getCreateDynamicCode() {
		Random random = new Random();
		StringBuilder dynamicCode = new StringBuilder();
		for (int i = 0; i < 6; i++) {
			dynamicCode.append(String.valueOf(random.nextInt(10)));
		}
		return dynamicCode.toString();
	}
}
