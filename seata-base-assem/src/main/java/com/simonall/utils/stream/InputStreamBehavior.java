package com.simonall.utils.stream;

import java.io.InputStream;

public interface InputStreamBehavior<R> {
	
	/**
	 * read Input Stream
	 * 
	 * @param inputStream
	 * @return
	 */
	R readInputStream(InputStream inputStream);

}
