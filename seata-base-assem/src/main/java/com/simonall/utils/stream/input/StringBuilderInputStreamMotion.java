package com.simonall.utils.stream.input;

import java.io.IOException;
import java.io.InputStream;

import com.simonall.utils.stream.InputStreamBehavior;

/**
 * 读取输入流到StringBuilder
 * 
 * @author simon
 */
public class StringBuilderInputStreamMotion implements InputStreamBehavior<StringBuilder> {

	@Override
	public StringBuilder readInputStream(InputStream inputStream) {
		StringBuilder result = new StringBuilder();
		try {
			int len = -1;
			byte[] bytes = new byte[8 * 1024];
			while ((len = inputStream.read(bytes)) != -1) {
				result.append(new String(bytes, 0, len));
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}

}
