package com.simonall.utils.encryption.rsa;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;

import com.simonall.utils.encryption.rsa.beans.SecretKeyModel;
import com.simonall.utils.encryption.rsa.behavior.RsaKeysBehavior;

public final class RsaKeys extends BaseRsa implements RsaKeysBehavior {
	
	/**
	 * 生成密钥对(公钥和私钥)
	 * 
	 * @return SecretKeyModel
	 */
	@Override
	public SecretKeyModel getSecretKey() throws Exception {
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(BaseRsa.KEYWORD);
		keyPairGen.initialize(1024);
		KeyPair keyPair = keyPairGen.generateKeyPair();
		
		SecretKeyModel model = new SecretKeyModel();
		// 公钥
		Key key = keyPair.getPublic();
		model.setPublicKey(encode(key.getEncoded()));
		
		// 私钥
		key = keyPair.getPrivate();
		model.setPrivateKey(encode(key.getEncoded()));
		
		return model;
	}
	
}