package com.simonall.utils.encryption.rsa.behavior;

import com.simonall.utils.encryption.rsa.beans.SecretKeyModel;

public interface RsaKeysBehavior {
	
	/**
	 * 生成公钥和私钥
	 * 
	 * @return
	 * @throws Exception
	 */
	public SecretKeyModel getSecretKey() throws Exception;
}
