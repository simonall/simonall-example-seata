package com.simonall.utils.encryption.rsa;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

/**
 * Ras加密
 * 
 * @author simon
 */
public class RsaEncrypt extends BaseRsa {
	
	/**
	 * 公钥加密
	 * 
	 * @param data
	 * 			源数据
	 * @param publicKey
	 * 			公钥
	 * @return
	 * @throws Exception
	 */
	public static String encryptByPub(final byte[] data, final String publicKey) throws Exception {
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(decode(publicKey));
		KeyFactory keyFactory = KeyFactory.getInstance(KEYWORD);
		Key publicK = keyFactory.generatePublic(x509KeySpec);
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicK);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			int offSet = 0;
			byte[] cache;
			int i = 0;
			// 数据分段加密
			int inputLen = data.length;
			while (inputLen - offSet > 0) {
				if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
					cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
				} else {
					cache = cipher.doFinal(data, offSet, inputLen - offSet);
				}
				out.write(cache, 0, cache.length);
				i++;
				offSet = i * MAX_ENCRYPT_BLOCK;
			}
			return encode(out.toByteArray());
		} finally {
			out.close();
		}
	}

	/**
	 * 私钥加密
	 * 
	 * @param data
	 * 			源数据
	 * @param privateKey
	 * 			私钥
	 * @return
	 * @throws Exception
	 */
	public String encryptByPri(final byte[] data, final String privateKey) throws Exception {
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(decode(privateKey));
		KeyFactory keyFactory = KeyFactory.getInstance(KEYWORD);
		Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, privateK);
		int inputLen = data.length;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			int offSet = 0;
			byte[] cache;
			int i = 0;
			// 数据分段加密
			while (inputLen - offSet > 0) {
				if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
					cache = cipher.doFinal(data, offSet, MAX_ENCRYPT_BLOCK);
				} else {
					cache = cipher.doFinal(data, offSet, inputLen - offSet);
				}
				out.write(cache, 0, cache.length);
				i++;
				offSet = i * MAX_ENCRYPT_BLOCK;
			}
			return encode(out.toByteArray());
		} finally {
			out.close();
		}
	}
	
}
