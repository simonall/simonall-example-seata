package com.simonall.utils.encryption.rsa.beans;

import java.io.Serializable;

public class SecretKeyModel implements Serializable {
	
	private static final long serialVersionUID = 2038694888615347227L;

	// 公钥
	private String publicKey;
	
	// 私钥
	private String privateKey;

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
}
