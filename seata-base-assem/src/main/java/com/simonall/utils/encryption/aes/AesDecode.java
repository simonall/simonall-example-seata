package com.simonall.utils.encryption.aes;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;

/**
 * AES 解密
 * 
 * @author simon
 */
public class AesDecode extends Aes {

	private static final long serialVersionUID = 8073725255695027450L;

	/**
	 * AES 解密操作
	 *
	 * @param content
	 *            待解密的密文
	 * @param password
	 *            密码
	 * @return
	 */
	public String decrypt(String content, String password) {
		try {
			Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, createSecretKey(password));
			byte[] result = cipher.doFinal(Base64.decodeBase64(content));
			return new String(result, "UTF-8");
		} catch (Exception ex) {

		}
		return null;
	}
}
