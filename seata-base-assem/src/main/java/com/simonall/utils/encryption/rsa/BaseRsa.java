package com.simonall.utils.encryption.rsa;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * Base RSA
 * 
 * @author simon
 */
public abstract class BaseRsa {
	
	/**
	 * 加密算法
	 * 
	 */
	public static final String KEYWORD = "RSA";
	
	/**
	 * 签名算法
	 */
	public static final String SIGNATURE_ALGORITHM = "MD5withRSA";
	
	/**
	 * RSA最大加密明文大小
	 * 
	 */
	public static final int MAX_ENCRYPT_BLOCK = 117;

	/**
	 * RSA最大解密密文大小
	 * 
	 */
	public static final int MAX_DECRYPT_BLOCK = 128;
	
	/**
	 * base64 加密
	 * 
	 * @param bytes
	 *            byte字符串
	 * @return String
	 * @throws Exception
	 *             异常
	 */
	public static String encode(final byte[] bytes) throws Exception {
		return new String(Base64.encodeBase64(bytes));
	}

	/**
	 * base64 解密
	 * 
	 * @param base64
	 *            base64 解密
	 * @return byte[]
	 * @throws Exception
	 *             异常
	 */
	public static byte[] decode(final String base64) throws Exception {
		return Base64.decodeBase64(base64.getBytes());
	}
	
	/**
	 * 用私钥对信息生成数字签名
	 * 
	 * @param data
	 *            	已加密数据
	 * @param privateKey
	 *            	私钥(BASE64编码)
	 * 
	 * @return String
	 * @throws Exception
	 *             异常
	 */
	public String sign(String data, String privateKey) throws Exception {
		byte[] keyBytes = decode(privateKey);
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEYWORD);
		PrivateKey privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initSign(privateK);
		signature.update(data.getBytes());
		return encode(signature.sign());
	}

	/**
	 * <h1>校验数字签名</h1>
	 * 
	 * @param data
	 *            已加密数据
	 * @param publicKey
	 *            公钥(BASE64编码)
	 * @param sign
	 *            数字签名
	 * @return boolean 签名校验结果
	 * @throws Exception
	 *             异常
	 */
	public boolean verifySign(String data, String publicKey, String sign) throws Exception {
		byte[] keyBytes = decode(publicKey);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEYWORD);
		PublicKey publicK = keyFactory.generatePublic(keySpec);
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initVerify(publicK);
		signature.update(data.getBytes());
		return signature.verify(decode(sign));
	}
}
