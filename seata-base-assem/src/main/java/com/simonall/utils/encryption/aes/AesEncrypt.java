package com.simonall.utils.encryption.aes;

import javax.crypto.Cipher;

import org.apache.commons.codec.binary.Base64;

/**
 * AES 加密对象
 * 
 * @author simon
 */
public class AesEncrypt extends Aes {

	private static final long serialVersionUID = 8896669052297108612L;

	private String content;

	private String password;

	public AesEncrypt(String content, String password) {
		this.content = content;
		this.password = password;
	}

	/**
	 * AES 加密操作
	 * 
	 * @param content
	 *            待加密内容
	 * @param password
	 *            加密密码
	 * @return 返回Base64转码后的加密数据
	 */
	public String encrypt() {
		try {
			// 创建密码器
			Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
			byte[] result = null;
			if (content instanceof String) {
				byte[] byteContent = String.valueOf(content).getBytes("UTF-8");
				cipher.init(Cipher.ENCRYPT_MODE, createSecretKey(password));
				result = cipher.doFinal(byteContent);
			}
			return Base64.encodeBase64String(result);
		} catch (Exception e) {

		}
		return null;
	}

}
