package com.simonall.utils.encryption.aes;

public class AesTest {

	public static void main(String[] args) {
		String content = "深圳市车投财富互联网金融服务有限公司";
		String password = String.valueOf(System.currentTimeMillis());

		// 加密
		AesEncrypt encrype = new AesEncrypt(content, password);
		String miwen = encrype.encrypt();

		System.out.println("打印密文：" + miwen);

		// 解密
		AesDecode decode = new AesDecode();
		String mingwen = decode.decrypt(miwen, password);

		System.out.println("打印明文：" + mingwen);
	}
}
