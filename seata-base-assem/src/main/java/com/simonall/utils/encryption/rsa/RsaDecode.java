package com.simonall.utils.encryption.rsa;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

/**
 * Rsa解密工具
 * 
 * @author simon
 */
public class RsaDecode extends BaseRsa {
	
	/**
	 * <h1>私钥解密</h1>
	 * 
	 * @param content
	 *            	已加密数据
	 * @param privateKey
	 *            	私钥(BASE64编码)
	 * @return byte[]
	 */
	public static String decryptByPri(final String content, final String privateKey) throws Exception {
		byte[] encryptedData = decode(content);

		// 创建密码器
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(decode(privateKey));
		KeyFactory keyFactory = KeyFactory.getInstance(KEYWORD);
		Key privateK = keyFactory.generatePrivate(pkcs8KeySpec);
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateK);

		// 创建字节流缓冲区
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			int inputLen = encryptedData.length;
			int offSet = 0;
			byte[] cache;
			int i = 0;
			// 对数据分段解密【因为数据长度限制】
			while (inputLen - offSet > 0) {
				if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
					cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
				} else {
					cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
				}
				out.write(cache, 0, cache.length);
				i++;
				offSet = i * MAX_DECRYPT_BLOCK;
			}
			return new String(out.toByteArray());
		} finally {
			out.close();
		}
	}

	/**
	 * <h1>公钥解密</h1>
	 * 
	 * @param content
	 *            	已加密数据
	 * @param publicKey
	 *           	 公钥(BASE64编码)
	 */
	public static String decryptByPub(final String content, final String publicKey) throws Exception {
		byte[] encryptedData = decode(content);
		
		// 创建密码器
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(decode(publicKey));
		KeyFactory keyFactory = KeyFactory.getInstance(KEYWORD);
		Key publicK = keyFactory.generatePublic(x509KeySpec);
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, publicK);

		// 创建字节流缓冲区
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			int offSet = 0;
			byte[] cache;
			int i = 0;
			// 数据分段解密
			int inputLen = encryptedData.length;
			while (inputLen - offSet > 0) {
				if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
					cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
				} else {
					cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
				}
				out.write(cache, 0, cache.length);
				i++;
				offSet = i * MAX_DECRYPT_BLOCK;
			}
			return new String(out.toByteArray());
		} finally {
			out.close();
		}
	}
}
