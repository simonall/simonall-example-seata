package com.simonall.utils.encryption.aes;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.simonall.utils.logs.LogUtil;

/**
 * AES
 * 
 * @author simonall
 */
public class Aes implements Serializable {

	private static final long serialVersionUID = 4175414935484145902L;

	// 加密方式
	private static final String KEY_ALGORITHM = "AES";

	// 默认的加密算法
	protected static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";

	/**
	 * 使用密码生成密码器
	 * 
	 * @param password
	 *            【外部传入】密码
	 * @return
	 */
	protected SecretKeySpec createSecretKey(final String password) {
		KeyGenerator kg = null;
		try {
			kg = KeyGenerator.getInstance(KEY_ALGORITHM);
			kg.init(128, new SecureRandom(password.getBytes()));
			SecretKey secretKey = kg.generateKey();
			return new SecretKeySpec(secretKey.getEncoded(), KEY_ALGORITHM);// 转换为AES专用密钥
		} catch (NoSuchAlgorithmException ex) {
			LogUtil.info(Aes.class.getName());
		}
		return null;
	}
}
