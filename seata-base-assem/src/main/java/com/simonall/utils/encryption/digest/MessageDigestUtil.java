package com.simonall.utils.encryption.digest;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.StringUtils;

/**
 * 【安全】消息摘要
 * 
 * @author simon
 */
public class MessageDigestUtil {

	/**
	 * MD5 32位消息摘要
	 * 
	 * @param content
	 * @return
	 */
	public static final String md5(String content) {
		if (!StringUtils.hasText(content)) {
			return content;
		}
		return DigestUtils.md5Hex(content);
	}
	
	/**
	 * MD5 16位消息摘要
	 * 
	 * @param content
	 * @return
	 */
	public static final String md5Half(String content) {
		if (!StringUtils.hasText(content)) {
			return content;
		}
		String md5Val = DigestUtils.md5Hex(content);
		return md5Val.substring(0, md5Val.length() / 2);
	}

	/**
	 * SHA-256消息摘要
	 * 
	 * @param content
	 * @return
	 */
	public static final String sha256(String content) {
		if (!StringUtils.hasText(content)) {
			return content;
		}
		return DigestUtils.sha256Hex(content);
	}

	/**
	 * SHA-512消息摘要
	 * 
	 * @param content
	 * @return
	 */
	public static final String sha512(String content) {
		if (!StringUtils.hasText(content)) {
			return content;
		}
		return DigestUtils.sha512Hex(content);
	}

}
