package com.simonall.utils.encryption.rsa;

import com.simonall.utils.encryption.rsa.beans.SecretKeyModel;

public class RsaTest {

	public static void main(String[] args) throws Exception {
		String content = "simonall.com";

		RsaKeys dddd = new RsaKeys();
		SecretKeyModel model = dddd.getSecretKey();
		// 公钥加密
		String miwen = RsaEncrypt.encryptByPub(content.getBytes(), model.getPublicKey());
		
		System.out.println("Base64密文=" + miwen);
		
		// 私钥解密
		String mingwen = RsaDecode.decryptByPri(miwen, model.getPrivateKey());
		System.out.println("解开明文" + mingwen);
		
		// 签名
		String sign = dddd.sign(content, model.getPrivateKey());
		System.out.println("sign=" + sign);
		
		// 验证签名
		System.out.println("验证签名结果：" + dddd.verifySign(content, model.getPublicKey(), sign));
	}

}
