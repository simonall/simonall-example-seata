package com.simonall.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

import com.simonall.constants.Value;

/**
 * Spring Context Handler
 * 
 * @author simon
 */
@Component
@Order(value = Integer.MIN_VALUE)
public class SpringContextUtil implements ApplicationContextAware {

	public static ApplicationContext context = null;

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		SpringContextUtil.context = context;
	}

	public static ApplicationContext getContext() {
		return context;
	}

	/**
	 * 使用注解Value值获取对象
	 * 
	 * @param annotationName
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(String annotationName) throws BeansException {
		if (context.containsBean(annotationName)) {
			return (T) context.getBean(annotationName);
		}
		return null;
	}

	/***
	 * 使用被注解的类类型获取该对象
	 */
	public static <T> T getBeanByClass(Class<T> beanClazz) throws BeansException {
		return context.getBean(beanClazz);
	}

	/**
	 * 获取项目根路径
	 * 
	 * @return
	 */
	public static String getPath() {
		return ClassUtils.getDefaultClassLoader().getResource(Value.BLANK).getPath();
	}

}
