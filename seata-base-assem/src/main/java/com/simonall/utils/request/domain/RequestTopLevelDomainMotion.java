package com.simonall.utils.request.domain;

import javax.servlet.http.HttpServletRequest;

import com.simonall.enums.CHS;

/**
 * 获取顶级域名 <br />
 * 举例子：www.ppmle.com 获取结果为：ppmle.com
 * @author simonall
 */
public class RequestTopLevelDomainMotion implements RequstServerName {

	@Override
	public String getDomain(HttpServletRequest request) {
		String domain = request.getServerName();
		if(domain != null && domain.indexOf(CHS.dit.getSymbol()) > -1) {
			String[] cutOut = domain.split(CHS.right_slash.getSymbol() + CHS.dit.getSymbol());
			domain = new String();
			for(int i = 1; i < cutOut.length; i++) {
				if(i == (cutOut.length - 1)) {
					domain += cutOut[i];
				}else {
					domain += cutOut[i] + CHS.dit.getSymbol();
				}
			}
		}
		return domain;
	}

}
