package com.simonall.utils.request.equipment;

import javax.servlet.http.HttpServletRequest;

public interface CheckEquipmentBehavior {

	/**
	 * 检查设备类型
	 * 
	 * @param request
	 * @return
	 */
	EquipmentTypeEnum check(HttpServletRequest request);

}
