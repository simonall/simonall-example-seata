package com.simonall.utils.request.xmlhttprequest;

import javax.servlet.http.HttpServletRequest;

public interface CheckXMLHttpRequestBehavior {

	/**
	 * 检查请求是否为ajax请求 <br/>
	 * 是：true <br/>
	 * 否：false
	 */
	boolean check(HttpServletRequest request);

}
