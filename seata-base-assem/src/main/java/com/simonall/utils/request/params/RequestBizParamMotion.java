package com.simonall.utils.request.params;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.simonall.enums.CHS;

/**
 * 获取请求头参数
 * 
 * @author simon
 */
public class RequestBizParamMotion implements RequestGetParamBehavior<Map<String, String>, HttpServletRequest> {

	@Override
	@SuppressWarnings("all")
	public Map<String, String> getRequestParam(HttpServletRequest request) {
		Map<String, String> paramsMap = new HashMap<String, String>();
		Map<String, String[]> properties = request.getParameterMap();
		Iterator entries = properties.entrySet().iterator();
		Map.Entry entry;
		while (entries.hasNext()) {
			String name = new String();
			String value = new String();
			entry = (Map.Entry) entries.next();
			name = (String) entry.getKey();
			Object object_value_data = entry.getValue();
			if (null == object_value_data) {
				value = new String();
			} else if (object_value_data instanceof String[]) {
				String[] list_request_values_data = (String[]) object_value_data;
				for (String val : list_request_values_data) {
					value += val + CHS.english_comma.getSymbol();
				}
				value = value.substring(0, value.length() - 1);
			} else {
				value = String.valueOf(object_value_data);
			}
			paramsMap.put(name, value);
		}
		return paramsMap;
	}

}
