package com.simonall.utils.request;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;

public class RequestUtil {

	/**
	 * 获取当前请求地址
	 * 
	 * @param request
	 * @return
	 */
	public static String getCurrentRequestUrl(HttpServletRequest request) {
		// 获取当前请求地址，当用户进行登录后直接跳转至目的地址
		StringBuilder currentRequestUrl = new StringBuilder(request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getRequestURI());
		String params = request.getQueryString();
		if (StringUtils.hasText(params)) {
			currentRequestUrl.append("?" + params);
		}
		return String.valueOf(currentRequestUrl);
	}

}
