package com.simonall.utils.request.params;

/**
 * 获取请求参数
 * 
 * @author simon
 */
public interface RequestGetParamBehavior<R, T> {

	/**
	 * 获取请求头参数
	 * 
	 * @param object
	 * @return
	 */
	R getRequestParam(T object);
}
