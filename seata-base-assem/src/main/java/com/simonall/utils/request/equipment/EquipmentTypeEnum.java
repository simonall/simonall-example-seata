package com.simonall.utils.request.equipment;

public enum EquipmentTypeEnum {

	/**
	 * 微信浏览器
	 * 
	 */
	WEIXIN_SOFTWARE,

	/**
	 * 手机浏览器
	 * 
	 */
	MOBILE_DEVICES,

	/**
	 * 电脑浏览器
	 * 
	 */
	COMPUTER_DEVICES
}
