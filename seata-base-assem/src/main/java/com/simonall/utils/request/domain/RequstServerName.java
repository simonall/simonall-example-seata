package com.simonall.utils.request.domain;

import javax.servlet.http.HttpServletRequest;

public interface RequstServerName {
	
	/**
	 * 获取域名
	 * 
	 * @param request
	 * @return
	 */
	String getDomain(HttpServletRequest request);
	
}
