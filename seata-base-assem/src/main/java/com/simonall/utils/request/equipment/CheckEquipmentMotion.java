package com.simonall.utils.request.equipment;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

/**
 * 检查设备类型
 * 
 * @author simon
 */
public class CheckEquipmentMotion implements CheckEquipmentBehavior {

	private static String phoneReg = "\\b(ip(hone|od)|android|opera m(ob|in)i" + "|windows (phone|ce)|blackberry" + "|s(ymbian|eries60|amsung)|p(laybook|alm|rofile/midp" + "|laystation portable)|nokia|fennec|htc[-_]" + "|mobile|up.browser|[1-4][0-9]{2}x[1-4][0-9]{2})\\b";
	private static String tableReg = "\\b(ipad|tablet|MIDP|SymbianOS|NOKIA|SAMSUNG|LG|NEC|TCL|Alcatel|BIRD|DBTEL|Dopod|PHILIPS|HAIER|LENOVO|MOT-|Nokia|SonyEricsson|SIE-|Amoi|ZTE|(Nexus 7)|up.browser" + "|[1-4][0-9]{2}x[1-4][0-9]{2})\\b";
	private static Pattern phonePat = Pattern.compile(phoneReg, Pattern.CASE_INSENSITIVE);
	private static Pattern tablePat = Pattern.compile(tableReg, Pattern.CASE_INSENSITIVE);

	@Override
	public EquipmentTypeEnum check(HttpServletRequest request) {
		String userAgent = request.getHeader("USER-AGENT");
		if (null == userAgent) {
			userAgent = new String();
		} else {
			userAgent = userAgent.toLowerCase();
		}
		Matcher matcherPhone = phonePat.matcher(userAgent);
		Matcher matcherTable = tablePat.matcher(userAgent);
		if (matcherPhone.find() || matcherTable.find()) {
			if (userAgent.indexOf("micromessenger") > 0) {// 是微信浏览器
				return EquipmentTypeEnum.WEIXIN_SOFTWARE;
			}
			return EquipmentTypeEnum.MOBILE_DEVICES;
		} else {
			return EquipmentTypeEnum.COMPUTER_DEVICES;
		}
	}

}
