package com.simonall.utils.data;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.simonall.constants.Value;
import com.simonall.enums.Keys;
import com.simonall.enums.ResponseCode;
import com.simonall.utils.gson.GsonSingleton;

/**
 * Response ResultData
 * 
 * @author simonall
 */
public class ResultData<T extends Object> implements Serializable {

	private static final long serialVersionUID = 3126376952008717753L;
	
	public ResultData() {
		super();
	}
	
	public ResultData(T data) {
		super();
		this.data = data;
	}

	// 响应码
	private String code = ResponseCode.SUCCESS.getCode();

	// 响应描述
	private String message = ResponseCode.SUCCESS.getMessage();

	// 响应结果
	private T data;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public T getData(Class<T> clazz) {
		if (data == null) {
			try {
				data = clazz.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
	@JsonIgnore
	public boolean isSuccess() {
		return Value.SUCCESS.equalsIgnoreCase(code);
	}

	public String toJson() {
		return GsonSingleton.getInstance().toJson(this);
	}
	
	public void setMessage(ResponseCode message) {
		this.setCode(message.getCode());
		this.setMessage(message.getMessage());
	}

	public void setMessage(Map<String, String> message) {
		this.setCode(message.get(Keys.code.name()));
		this.setMessage(message.get(Keys.message.name()));
	}

	public void setMessage(String code, String message) {
		this.setCode(code);
		this.setMessage(message);
	}
	
	public static ResultData<?> SUCCESS(final ResponseCode message) {
		ResultData<Object> result = new ResultData<Object>();
		result.setMessage(message);
		return result;
	}
	
	public static ResultData<?> SUCCESS(final String code, final String message) {
		ResultData<Object> result = new ResultData<Object>();
		result.setMessage(code, message);
		return result;
	}
	
	public static ResultData<?> SUCCESS(final Object data) {
		return new ResultData<Object>(data);
	}
	
	public static ResultData<?> SUCCESS() {
		return new ResultData<Object>();
	}
}
