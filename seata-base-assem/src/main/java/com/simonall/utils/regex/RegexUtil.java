package com.simonall.utils.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class RegexUtil {

	/**
	 * 验证邮箱
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean isEmail(final String context) {
		String regex = "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		return match(regex, context);
	}

	/**
	 * 验证IP地址
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean isIP(final String context) {
		String num = "(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)";
		String regex = "^" + num + "\\." + num + "\\." + num + "\\." + num + "$";
		return match(regex, context);
	}

	/**
	 * 验证网址Url
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsUrl(final String context) {
		String regex = "http(s)?://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";
		return match(regex, context);
	}

	/**
	 * 验证电话号码
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsTelephone(final String context) {
		String regex = "^(\\d{3,4}-)?\\d{6,8}$";
		return match(regex, context);
	}

	/**
	 * 验证输入密码条件(字符与数据同时出现)
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsPassword(final String context) {
		String regex = "[A-Za-z]+[0-9]";
		return match(regex, context);
	}

	/**
	 * 验证输入密码长度 (6-18位)
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsPasswLength(final String context) {
		String regex = "^\\d{6,18}$";
		return match(regex, context);
	}

	/**
	 * 验证输入邮政编号
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsPostalcode(final String context) {
		String regex = "^\\d{6}$";
		return match(regex, context);
	}

	/**
	 * 验证输入手机号码
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsHandset(final String context) {
		String regex = "^[1]+[3,5]+\\d{9}$";
		return match(regex, context);
	}

	/**
	 * 验证输入身份证号
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsIDcard(final String context) {
		String regex = "(^\\d{18}$)|(^\\d{15}$)";
		return match(regex, context);
	}

	/**
	 * 验证输入两位小数
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsDecimal(final String context) {
		String regex = "^[0-9]+(.[0-9]{2})?$";
		return match(regex, context);
	}

	/**
	 * 验证输入一年的12个月
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsMonth(final String context) {
		String regex = "^(0?[[1-9]|1[0-2])$";
		return match(regex, context);
	}

	/**
	 * 验证输入一个月的31天
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsDay(final String context) {
		String regex = "^((0?[1-9])|((1|2)[0-9])|30|31)$";
		return match(regex, context);
	}

	/**
	 * 验证日期时间
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合网址格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean isDate(final String context) {
		String regex = "^((((1[6-9]|[2-9]\\d)\\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\\d|3[01]))|(((1[6-9]|[2-9]\\d)\\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\\d|30))|(((1[6-9]|[2-9]\\d)\\d{2})-0?2-(0?[1-9]|1\\d|2[0-8]))|(((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-)) (20|21|22|23|[0-1]?\\d):[0-5]?\\d:[0-5]?\\d$";
		return match(regex, context);
	}

	/**
	 * 验证数字输入
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsNumber(final String context) {
		String regex = "^[0-9]*$";
		return match(regex, context);
	}

	/**
	 * 验证非零的正整数
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsIntNumber(final String context) {
		String regex = "^\\+?[1-9][0-9]*$";
		return match(regex, context);
	}

	/**
	 * 验证大写字母
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsUpChar(final String context) {
		String regex = "^[A-Z]+$";
		return match(regex, context);
	}

	/**
	 * 验证小写字母
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsLowChar(final String context) {
		String regex = "^[a-z]+$";
		return match(regex, context);
	}

	/**
	 * 验证验证输入字母
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsLetter(final String context) {
		String regex = "^[A-Za-z]+$";
		return match(regex, context);
	}

	/**
	 * 验证验证输入汉字
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsChinese(final String context) {
		String regex = "^[\u4e00-\u9fa5]{0,}$";
		return match(regex, context);
	}

	/**
	 * 验证验证输入字符串
	 * 
	 * @param context 待验证的字符串
	 * @return 如果是符合格式的字符串,返回 <b>true </b>,否则为 <b>false </b>
	 */
	public static boolean IsLength(final String context) {
		String regex = "^.{8,}$";
		return match(regex, context);
	}

	/**
	 * @param regex   正则表达式字符串
	 * @param context 要匹配的字符串
	 * @return 符合 regex的正则表达式格式,返回true, 否则返回 false;
	 */
	private static boolean match(final String regex, final String context) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(context);
		return matcher.matches();
	}

}
