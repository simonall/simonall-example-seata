package com.simonall.utils.date.compute.motion;

import java.util.Date;

import com.simonall.utils.date.compute.behavior.DateDiffBehavior;
import com.simonall.utils.logs.LogUtil;

/**
 * 计算两个时间之间时间间隔
 * 
 * @author simonall
 */
public class DateDiffMotion implements DateDiffBehavior {

	@Override
	public Long dateDiffDays(Date beginTime, Date endTime) {
		Long beginTimestamp = null;
		if(beginTime == null) {
			LogUtil.error(" Class DateDiffMotion#dateDiffDays beginTime NullException ");
		}else {
			beginTimestamp = beginTime.getTime();
		}
		Long endTimestamp = null;
		if(endTime == null) {
			LogUtil.error(" Class DateDiffMotion#dateDiffDays endTime NullException ");
		}else {
			endTimestamp = endTime.getTime();
		}
		if(beginTimestamp == null || endTimestamp == null) {
			return null;
		}
		return (endTimestamp - beginTimestamp) / 86400000;
	}

	@Override
	public Long dateDiffHour(Date beginTime, Date endTime) {
		Long beginTimestamp = null;
		if(beginTime == null) {
			LogUtil.error(" Class DateDiffMotion#dateDiffHour beginTime NullException ");
		}else {
			beginTimestamp = beginTime.getTime();
		}
		Long endTimestamp = null;
		if(endTime == null) {
			LogUtil.error(" Class DateDiffMotion#dateDiffHour endTime NullException ");
		}else {
			endTimestamp = endTime.getTime();
		}
		if(beginTimestamp == null || endTimestamp == null) {
			return null;
		}
		return (endTimestamp - beginTimestamp) / 3600000;
	}

	@Override
	public Long dateDiffMinute(Date beginTime, Date endTime) {
		Long beginTimestamp = null;
		if(beginTime == null) {
			LogUtil.error(" Class DateDiffMotion#dateDiffMinute beginTime NullException ");
		}else {
			beginTimestamp = beginTime.getTime();
		}
		Long endTimestamp = null;
		if(endTime == null) {
			LogUtil.error(" Class DateDiffMotion#dateDiffMinute endTime NullException ");
		}else {
			endTimestamp = endTime.getTime();
		}
		if(beginTimestamp == null || endTimestamp == null) {
			return null;
		}
		return (endTimestamp - beginTimestamp) / 60000;
	}

	@Override
	public Long dateDiffSecond(Date beginTime, Date endTime) {
		Long beginTimestamp = null;
		if(beginTime == null) {
			LogUtil.error(" Class DateDiffMotion#dateDiffSecond beginTime NullException ");
		}else {
			beginTimestamp = beginTime.getTime();
		}
		Long endTimestamp = null;
		if(endTime == null) {
			LogUtil.error(" Class DateDiffMotion#dateDiffSecond endTime NullException ");
		}else {
			endTimestamp = endTime.getTime();
		}
		if(beginTimestamp == null || endTimestamp == null) {
			return null;
		}
		return (endTimestamp - beginTimestamp) / 1000;
	}

}
