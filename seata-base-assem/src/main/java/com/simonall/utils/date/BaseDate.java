package com.simonall.utils.date;

import java.util.Calendar;
import java.util.Date;

/**
 * 基础时间对象
 * 
 * @author simon
 */
public abstract class BaseDate {

	public Date createDate() {

		return new Date();
	}

	public Calendar getCalendar() {

		return Calendar.getInstance();
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static Date getNowTime() {

		return new Date();
	}

}
