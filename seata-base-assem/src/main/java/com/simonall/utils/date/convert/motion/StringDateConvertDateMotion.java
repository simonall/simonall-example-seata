package com.simonall.utils.date.convert.motion;

import java.text.DateFormat;
import java.util.Date;

import com.simonall.utils.date.convert.behavior.DateConvertBehavior;
import com.simonall.utils.logs.LogUtil;

/**
 * 时间格式字符串转时间【Date】
 * 
 * @author simon
 */
public class StringDateConvertDateMotion implements DateConvertBehavior<Date, String> {

	@Override
	public Date convert(DateFormat format, String source) {
		Date result = null;
		if (format != null && source != null) {
			try {
				result = format.parse(source);
			} catch (Exception e) {
				LogUtil.info(" String Date Convert Date Exception " + e.getMessage());
			}
		}
		return result;
	}


}
