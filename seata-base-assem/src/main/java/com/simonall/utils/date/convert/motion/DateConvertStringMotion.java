package com.simonall.utils.date.convert.motion;

import java.text.DateFormat;
import java.util.Date;

import com.simonall.utils.date.convert.behavior.DateConvertBehavior;

/**
 * 日期类型转字符串日期
 * 
 * @author simon
 */
public class DateConvertStringMotion implements DateConvertBehavior<String, Date> {

	@Override
	public String convert(final DateFormat format, final Date source) {
		String result = new String();
		if (format != null && source != null) {
			result = format.format(source);
		}
		return result;
	}

}
