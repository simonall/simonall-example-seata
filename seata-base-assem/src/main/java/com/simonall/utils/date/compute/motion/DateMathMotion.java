package com.simonall.utils.date.compute.motion;

import java.util.Calendar;
import java.util.Date;

import com.simonall.utils.date.compute.behavior.DateMathBehavior;
import com.simonall.utils.logs.LogUtil;

/**
 * 时间/日期数学运算
 * 
 * @author simonall
 */
public class DateMathMotion implements DateMathBehavior {
	
	@Override
	public Date dateAddSecond(Date setTime, Integer second) {
		if(setTime == null) {
			LogUtil.error(" Class DateMathMotion#dateAddSecond setTime Param NullException ");
		}
		if(second == null) {
			LogUtil.error(" Class DateMathMotion#dateAddSecond second Param NullException ");
		}
		if(setTime == null || second == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(setTime);
		calendar.add(Calendar.SECOND, second.intValue());
		return calendar.getTime();
	}
	
	@Override
	public Date dateAddMonth(Date setTime, Integer months) {
		if(setTime == null) {
			LogUtil.error(" Class DateMathMotion#dateAddMonth setTime Param NullException ");
		}
		if(months == null) {
			LogUtil.error(" Class DateMathMotion#dateAddMonth months Param NullException ");
		}
		if(setTime == null || months == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(setTime);
		calendar.add(Calendar.MONTH, months.intValue());
		return calendar.getTime();
	}

	@Override
	public Date dateAddDay(Date setTime, Integer days) {
		if(setTime == null) {
			LogUtil.error(" Class DateMathMotion#dateAddDay setTime Param NullException ");
		}
		if(days == null) {
			LogUtil.error(" Class DateMathMotion#dateAddDay days Param NullException ");
		}
		if(setTime == null || days == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(setTime);
		calendar.add(Calendar.DAY_OF_YEAR, days.intValue());
		return calendar.getTime();
	}

	@Override
	public Date dateAddYear(Date setTime, Integer years) {
		if(setTime == null) {
			LogUtil.error(" Class DateMathMotion#dateAddYear setTime Param NullException ");
		}
		if(years == null) {
			LogUtil.error(" Class DateMathMotion#dateAddYear years Param NullException ");
		}
		if(setTime == null || years == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(setTime);
		calendar.add(Calendar.YEAR, years.intValue());
		return calendar.getTime();
	}

	@Override
	public Date dateAddMinute(Date setTime, Integer minute) {
		if(setTime == null) {
			LogUtil.error(" Class DateMathMotion#dateAddMinute setTime Param NullException ");
		}
		if(minute == null) {
			LogUtil.error(" Class DateMathMotion#dateAddMinute minute Param NullException ");
		}
		if(setTime == null || minute == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(setTime);
		calendar.add(Calendar.MINUTE, minute.intValue());
		return calendar.getTime();
	}

}
