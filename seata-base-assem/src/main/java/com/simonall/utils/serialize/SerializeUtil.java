package com.simonall.utils.serialize;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.apache.tomcat.util.codec.binary.Base64;

import com.simonall.utils.logs.LogUtil;

public class SerializeUtil {

	private Object value;

	public SerializeUtil(Object value) {
		this.value = value;
	}

	/**
	 * 序列化内容为二进制Byte
	 * 
	 * @return byte
	 * @throws IOException
	 */
	public byte[] getByte() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		try {
			oos.writeObject(value);
			return baos.toByteArray();
		} catch (Exception e) {
			LogUtil.info("Object serialize Exection " + e.getMessage());
		} finally {
			baos.flush();
			baos.close();
			oos.close();
		}
		return new byte[0];
	}

	/**
	 * 序列化对象并返回Base64编译后的译文
	 * 
	 * @return
	 */
	public String getBase64String() {
		String result = new String();
		try {
			result = Base64.encodeBase64String(getByte());
		} catch (IOException e) {
			LogUtil.info("Base64 Byte[] To String Exection " + e.getMessage());
		}
		return result;
	}

}
