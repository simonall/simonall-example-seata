package com.simonall.utils.serialize;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.simonall.utils.logs.LogUtil;

/**
 * 反序列化
 * 
 * @author simon
 */
public class UnSerializeUtil {

	private byte[] value;

	/**
	 * 将字符串进行Base64解码
	 * 
	 * @param value
	 */
	public UnSerializeUtil(byte[] value) {
		this.value = value;
	}

	/**
	 * 将Value反序列化Object
	 * 
	 * @return
	 */
	public Object getObject() {
		ByteArrayInputStream input = new ByteArrayInputStream(this.value);
		try {
			ObjectInputStream stream = new ObjectInputStream(input);
			return stream.readObject();
		} catch (Exception e) {
			LogUtil.info("Object Value UnSerialize-result-object Exception " + e.getMessage());
		}
		return null;
	}

	/**
	 * 将Value反序列化Map
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String> getMap() {
		ByteArrayInputStream input = new ByteArrayInputStream(this.value);
		try {
			ObjectInputStream stream = new ObjectInputStream(input);
			return (Map<String, String>) stream.readObject();
		} catch (Exception e) {
			LogUtil.info("Object Value UnSerialize-result-map Exception " + e.getMessage());
		}
		return new HashMap<>();
	}

	/**
	 * 将Value反序列化List
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getList() {
		ByteArrayInputStream input = new ByteArrayInputStream(this.value);
		try {
			ObjectInputStream stream = new ObjectInputStream(input);
			return (List<Object>) stream.readObject();
		} catch (Exception e) {
			LogUtil.info("Object Value UnSerialize-result-List Exception " + e.getMessage());
		}
		return new ArrayList<>();
	}
}
