package com.simonall.utils.logs;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.simonall.enums.CHS;
import com.simonall.utils.date.BaseDate;
import com.simonall.utils.date.convert.config.DateFormatConfig;
import com.simonall.utils.date.convert.motion.DateConvertStringMotion;

/**
 * 日志工具类
 * 
 * @author simon
 */
public class LogUtil {
	
	public static Logger logger = Logger.getLogger(LogUtil.class);
	
	public static DateConvertStringMotion motion = new DateConvertStringMotion();

	/**
	 * 打印 info 日志信息
	 * 
	 * @param message
	 *            内容
	 */
	public static void info(Object message) {
		
		logger.info(motion.convert(DateFormatConfig.YYYYMMDDMMHHSS(), BaseDate.getNowTime()) + CHS.equal.getSymbol() + message);
	}

	/**
	 * 打印错误 ERROR 日志信息
	 * 
	 * @param message
	 *            内容
	 */
	public static void error(Object message) {

		logger.error(motion.convert(DateFormatConfig.YYYYMMDDMMHHSS(), BaseDate.getNowTime()) + CHS.equal.getSymbol() + message);
	}

	/**
	 * 打印 警告 （warn） 日志
	 * 
	 * @param message
	 *            内容
	 */
	public static void warn(Object message) {

		logger.warn(motion.convert(DateFormatConfig.YYYYMMDDMMHHSS(), BaseDate.getNowTime()) + CHS.equal.getSymbol() + message);
	}

	/**
	 * 打印 debug 日志信息
	 * 
	 * @param message
	 *            内容
	 */
	public static void debug(Object message) {

		logger.debug(motion.convert(DateFormatConfig.YYYYMMDDMMHHSS(), BaseDate.getNowTime()) + CHS.equal.getSymbol() + message);
	}

	/**
	 * 打印 调试 日志信息
	 * 
	 * @param message
	 *            内容
	 */
	public static void trace(Object message) {

		logger.trace(motion.convert(DateFormatConfig.YYYYMMDDMMHHSS(), BaseDate.getNowTime()) + CHS.equal.getSymbol() + message);
	}

	/**
	 * 使用info 打印MAP参数集合
	 * 
	 * @param map
	 *            参数集合
	 */
	public static void infoPrintMap(Map<String, String> map) {
		if (map == null || map.size() == 0) {
			return;
		}
		LogUtil.info("--------------------------- 开始打印参数 ---------------------------");
		for (Entry<String, String> entry : map.entrySet()) {
			if (entry.getValue() != null) {
				LogUtil.info(motion.convert(DateFormatConfig.YYYYMMDDMMHHSS(), BaseDate.getNowTime()) + CHS.equal.getSymbol() + "***********" + entry.getKey() + "--" + entry.getValue());
			}
		}
		LogUtil.info("--------------------------- 结束打印参数 ----------------------------\n");
	}

	/**
	 * 使用info 打印MAP参数集合
	 * 
	 * @param map
	 *            参数集合
	 */
	public static void infoPrintObjMap(Map<String, Object> map) {
		if (map == null || map.size() == 0) {
			return;
		}
		LogUtil.info("--------------------------- 开始打印参数 ---------------------------");
		for (Entry<String, Object> entry : map.entrySet()) {
			if (entry.getValue() != null) {
				LogUtil.info(motion.convert(DateFormatConfig.YYYYMMDDMMHHSS(), BaseDate.getNowTime()) + CHS.equal.getSymbol() + "***********" + entry.getKey() + "--" + entry.getValue());
			}
		}
		LogUtil.info("--------------------------- 结束打印参数 ---------------------------\n");
	}

}
