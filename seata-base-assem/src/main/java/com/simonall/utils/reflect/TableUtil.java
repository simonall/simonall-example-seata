package com.simonall.utils.reflect;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;

import com.simonall.constants.Value;
import com.simonall.enums.CHS;

public class TableUtil {
	
	public static Set<Class<?>> getClasses(String packagePath) {
		Set<Class<?>> result = new LinkedHashSet<Class<?>>();
		String packageName = packagePath;
		String packageDirPath = packageName.replace(CHS.dit.getSymbol(), CHS.left_slash.getSymbol());
		try {
			Enumeration<URL> dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirPath);
			while (dirs.hasMoreElements()) {
				URL url = dirs.nextElement();
				String filePath = URLDecoder.decode(url.getFile(), Value.CHARSET);
				packageByFile(packageName, filePath, true, result);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void packageByFile(String packageName, String packagePath, final boolean recursive, Set<Class<?>> result) {
		File dir = new File(packagePath);
		if (!dir.exists() || !dir.isDirectory()) {
			return;
		}
		File[] dirfiles = dir.listFiles(new FileFilter() {
			public boolean accept(File file) {
				return (recursive && file.isDirectory()) || (file.getName().endsWith(".class"));
			}
		});
		for (File file : dirfiles) {
			if (file.isDirectory()) {
				packageByFile(packageName + CHS.dit.getSymbol() + file.getName(), file.getAbsolutePath(), recursive, result);
			} else {
				String className = file.getName().substring(0, file.getName().length() - 6);
				try {
					result.add(Thread.currentThread().getContextClassLoader().loadClass(packageName + CHS.dit.getSymbol() + className));
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
}
