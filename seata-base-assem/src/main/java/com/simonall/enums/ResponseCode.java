package com.simonall.enums;

public enum ResponseCode {
	
	
	SUCCESS;
	
	private String code = this.name();
	
	private String message;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
