package com.simonall.exception;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.simonall.enums.ResponseCode;
import com.simonall.enums.Keys;

/**
 * Business IllegalArgument Exception
 * 
 * @author simonall
 */
public class BusinessIllegalException extends IllegalArgumentException {

	private static final long serialVersionUID = -4937908249135897289L;

	/**
	 * 单参数构造器
	 * 
	 * @param message
	 */
	public BusinessIllegalException(String message) {
		super(message);
	}

	/**
	 * 完全参数构造器
	 * 
	 * @param code
	 * @param message
	 */
	public BusinessIllegalException(String code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	/**
	 * 完全参数构造器
	 * 
	 * @param code
	 * @param message
	 */
	public BusinessIllegalException(ResponseCode message) {
		super(message.getMessage());
		this.code = message.name();
		this.message = message.getMessage();
	}

	private String code;

	private String message;

	public final String toJson() {
		return JSON.toJSONString(toMap());
	}

	public final Map<String, String> toMap() {
		Map<String, String> result = new HashMap<String, String>();
		result.put(Keys.code.name(), this.code);
		result.put(Keys.message.name(), this.message);
		return result;
	}

}
