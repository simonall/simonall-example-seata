package com.simonall.service.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.simonall.service.SystemLogBusService;
import com.simonall.service.SystemLogService;
import com.simonall.system.model.SystemLog;

@Service(value = "systemLogBusService")
public class SystemLogBusServiceImpl implements SystemLogBusService {
	
	@Resource
	private SystemLogService systemLogService;

	@Override
	@Transactional
	public void createSystemLog() {
		SystemLog entity = new SystemLog();
		entity.setCreateTime(new Date());
		entity.setRequestUrl("/index.html");
		entity.setIp("192.168.1.1");
		systemLogService.save(entity);
	}
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void getSystemLog() {
		SystemLog systemLog = systemLogService.getById("fd6408a4-47cf-4e72-8aa2-b287381ef792");
		System.out.println(systemLog.toString());
		
		systemLog = systemLogService.getById("fd6408a4-47cf-4e72-8aa2-b287381ef792");
		System.out.println(systemLog.toString());
		
		this.createSystemLog();
		
		systemLog = systemLogService.getById("fd6408a4-47cf-4e72-8aa2-b287381ef792");
		System.out.println(systemLog.toString());
	}
	
	
	
}
