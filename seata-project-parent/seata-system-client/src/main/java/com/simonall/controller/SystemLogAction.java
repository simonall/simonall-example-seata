package com.simonall.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simonall.service.SystemLogBusService;
import com.simonall.service.SystemLogService;

@RestController
public class SystemLogAction {
	
	@Resource
	private SystemLogBusService systemLogBusService;
	
	@Resource
	private SystemLogService systemLogService;

	@GetMapping(value = "/createSystemLog")
	public void createSystemLog() {
		try {
			systemLogBusService.createSystemLog();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@GetMapping(value = "/getSystemLog")
	public void getSystemLog() {
		systemLogBusService.getSystemLog();
	}
	
}
