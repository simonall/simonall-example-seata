package com.simonall.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.simonall.service.UserBusService;

@RestController
public class UserAction {
	
	
	@Resource
	private UserBusService userBusService;
	
	@GetMapping(value = "/createUser")
	public void createUser() {
		
		userBusService.createUser();
	}

}
