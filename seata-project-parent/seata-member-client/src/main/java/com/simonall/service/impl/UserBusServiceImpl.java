package com.simonall.service.impl;

import java.util.Date;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.simonall.feign.SystemFeignClient;
import com.simonall.member.model.User;
import com.simonall.service.UserBusService;
import com.simonall.service.UserService;

import io.seata.spring.annotation.GlobalTransactional;

@Service(value = "userBusService")
public class UserBusServiceImpl implements UserBusService {
	
	@Resource
	private UserService userService;
	
	@Resource
	private SystemFeignClient systemFeignClient;
	
	@Override
	@Transactional(rollbackFor = Exception.class)
	@GlobalTransactional
	public void createUser() {
		User user = new User();
		user.setCreateTime(new Date());
		user.setUserName(UUID.randomUUID().toString());
		user.setNickName(UUID.randomUUID().toString());
		userService.save(user);
		
		systemFeignClient.createSystemLog();
		
		int i = 100 / 0;
	}
	
	

}
