package com.simonall;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableFeignClients
@ServletComponentScan
@EnableDiscoveryClient
@SpringBootApplication
@EnableTransactionManagement
public class MemberClientApp implements WebMvcConfigurer {
	
	public static void main(String[] args) {
		SpringApplication.run(MemberClientApp.class, args);
	}
	
	/**
	 * sevlet-url-pattern
	 * 
	 */
	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		configurer.setUseSuffixPatternMatch(true).setUseTrailingSlashMatch(true);
	}
	
}
