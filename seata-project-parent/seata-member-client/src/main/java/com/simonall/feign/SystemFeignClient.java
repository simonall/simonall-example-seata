package com.simonall.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "seata-system-client")
public interface SystemFeignClient {
	
	@GetMapping("/createSystemLog")
    void createSystemLog();

}
