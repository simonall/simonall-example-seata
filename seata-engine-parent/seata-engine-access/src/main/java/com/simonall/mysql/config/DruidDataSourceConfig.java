package com.simonall.mysql.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.annotation.Order;

@Configuration
@Order(value = Integer.MIN_VALUE)
@ImportResource(locations = { "classpath:druid-data-source.xml" })
public class DruidDataSourceConfig {
	
}
