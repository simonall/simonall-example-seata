package com.simonall.mysql.source;

import java.util.Random;

/**
 * 设置具体使用的数据库实例
 * 
 * @author JinQuan
 */
public class ReadWriteIsolateFactory {

	private static final String[] mainDataSources = { "mainDataSource" };
	private static final String[] readDataSources = { "readDataSource" };
	private static final Random random = new Random();

	public static void setWriteDataSource() {
		Integer index = random.nextInt((mainDataSources.length));
		DataSourceHandler.setDataSource(mainDataSources[index]);
	}

	public static void setReadDataSource() {
		Integer index = random.nextInt((readDataSources.length));
		DataSourceHandler.setDataSource(readDataSources[index]);
	}

}
