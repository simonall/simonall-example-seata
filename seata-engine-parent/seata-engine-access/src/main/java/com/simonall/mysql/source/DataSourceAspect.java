package com.simonall.mysql.source;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 读取实例切面
 * 
 * @author JinQuan
 */
@Aspect
@Order(3)
@Component
public class DataSourceAspect {

	/**
	 * 声明切面类型
	 * 
	 * @see org.springframework.transaction.annotation.Transactional
	 */
	@Pointcut("@within(org.springframework.transaction.annotation.Transactional) || @annotation(org.springframework.transaction.annotation.Transactional)")
	public void aspect() {
	}

	@Before(value = "aspect()")
	public void doBefore(JoinPoint joinPoint) {
		Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
		// 获取方法上的注解
		Transactional annotation = method.getAnnotation(Transactional.class);
		if (annotation == null) {
			// 尝试获取类上面的注解
			annotation = joinPoint.getTarget().getClass().getAnnotation(Transactional.class);
			if (annotation == null)
				return;
		}
		// 获取注解上的数据源的值的信息
		boolean readOnly = annotation.readOnly();
		if (readOnly) {
			// 只读实例
			ReadWriteIsolateFactory.setReadDataSource();
		} else {
			// 只写实例
			ReadWriteIsolateFactory.setWriteDataSource();
		}
	}

	@After(value = "aspect()")
	public void after(JoinPoint point) {
		// 清空当前线程设置
		DataSourceHandler.clear();
	}

}
