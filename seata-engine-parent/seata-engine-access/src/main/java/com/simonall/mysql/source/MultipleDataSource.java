package com.simonall.mysql.source;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 动态切换数据源
 * 
 * @author Simonall
 */
public class MultipleDataSource extends AbstractRoutingDataSource {

	/**
	 * 重写获取数据源Key的方法
	 * 
	 * @see AbstractRoutingDataSource#determineTargetDataSource()
	 */
	@Override
	protected Object determineCurrentLookupKey() {
		System.out.println("Get = " + DataSourceHandler.getDataSource());
		return DataSourceHandler.getDataSource();
	}

}
