package com.simonall.mysql.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.simonall.utils.page.Page;

public interface BaseDao<T> {

	/**
	 * 保存数据
	 * 
	 * @param entity 实体对象
	 */
	void save(T entity);

	/**
	 * 删除数据
	 * 
	 * @param id Id
	 */
	void deleteById(String... id);

	/**
	 * 更新数据
	 * 
	 * @param entity 实体对象
	 */
	void update(T entity);

	/**
	 * 保存和更新数据
	 * 
	 * @param entity 实体对象
	 */
	void saveOrUpdate(T entity);

	/**
	 * 根据ID查看记录
	 * 
	 * @param id Id
	 * @return 实体对象
	 */
	T getById(Serializable id);

	/**
	 * 查询某个对象记录集合
	 * 
	 * @return List 集合
	 */
	List<T> list();

	/**
	 * 根据属性名和属性值获取实体对象.
	 * 
	 * @param field 字段
	 * @param value 属性值
	 */
	T get(String field, Object value);

	/**
	 * 根据属性名和属性值获取实体对象集合.
	 * 
	 * @param field 字段
	 * @param value 属性值
	 */
	List<T> list(String field, Object value);

	/**
	 * 根据属性名判断数据是否已存在.
	 * 
	 * @param field 字段
	 * @param value 值
	 */
	boolean isExist(String field, Object value);

	/**
	 * HQL查询数据集合
	 * 
	 * @param hql    HQL语句
	 * @param values 查询条件
	 */
	List<T> listByHql(StringBuilder hql, Map<String, Object> values);

	/**
	 * HQL获取唯一对象的方法
	 * 
	 * @param hql    HQL语句
	 * @param values 查询条件
	 */
	T getUniqueByHql(StringBuilder hql, Map<String, Object> values);

	/**
	 * HQL获取唯一对象的方法
	 * 
	 * @param hql    HQL语句
	 * @param values 查询条件
	 */
	<R> R getUniqueObjectByHql(StringBuilder hql, Map<String, Object> values);

	/**
	 * HQL分页获取对象集合的方法
	 * 
	 * @param hql    HQL语句
	 * @param values 查询条件
	 */
	Page<T> pageByHql(StringBuilder hql, Map<String, Object> values);

	/**
	 * SQL获取唯一对象的
	 * 
	 * @param sql    SQL语句
	 * @param values 查询条件
	 */
	T getUniqueBySql(StringBuilder sql, Map<String, Object> values);

	/**
	 * SQL查询数据集合
	 * 
	 * @param sql    SQL语句
	 * @param values 查询条件
	 * @return 集合
	 */
	List<T> listBySql(StringBuilder sql, Map<String, Object> values);

	/**
	 * SQL分页获取对象集合的方法
	 * 
	 * @param sql    SQL语句
	 * @param values 查询条件
	 */
	Page<T> pageBySql(StringBuilder sql, Map<String, Object> values);

	/**
	 * SQL查询任意唯一结果
	 * 
	 * @param sql    SQL语句
	 * @param values 条件查询值
	 * @return R 由调用者觉得是什么类型
	 */
	<R> R getUniqueObjectBySql(StringBuilder sql, Map<String, Object> values);

	/**
	 * 【泛型Bean】 查询记录
	 * 
	 * @param sql    SQL语句
	 * @param values 条件查询
	 * @param R      由调用者觉得是什么类型
	 * @return
	 */
	<R> R getUniqueGenericsBeanBySql(StringBuilder sql, Map<String, Object> values, Class<?> R);

	/**
	 * 【泛型Bean】查询记录集合
	 * 
	 * @param sql    SQL语句
	 * @param R      返回对象类型
	 * @param values 条件查询值
	 * @return List
	 */
	<R> R listGenericsBeanBySql(StringBuilder sql, Map<String, Object> values, Class<?> R);

	/**
	 * 【泛型Bean】分页查询分页记录
	 * 
	 * @param sql    SQL语句
	 * @param page   分页对象
	 * @param R      返回对象类型
	 * @param values 条件查询值
	 * @return page
	 */
	<R> R pageGenericsBeanBySql(StringBuilder sql, Map<String, Object> values, Class<?> R);

	/**
	 * 执行一条SQL语句
	 * 
	 * @param sql    SQL语句
	 * @param values 分页对象
	 * @return
	 */
	int executeBySql(StringBuilder sql, Map<String, Object> values);

	/**
	 * 根据属性名更新属性值
	 * 
	 * @param field  字段名称
	 * @param origin 源数据
	 * @param target 目标数据
	 */
	void updateFieldValueBySql(String field, Object origin, Object target);

}
