package com.simonall.dao.impl;

import org.springframework.stereotype.Repository;

import com.simonall.dao.SystemLogDao;
import com.simonall.mysql.dao.impl.BaseDaoImpl;
import com.simonall.system.model.SystemLog;

@Repository(value = "systemLogDao")
public class SystemLogDaoImpl extends BaseDaoImpl<SystemLog> implements SystemLogDao {
	
}
