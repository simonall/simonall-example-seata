package com.simonall.dao;

import com.simonall.mysql.dao.BaseDao;
import com.simonall.system.model.SystemLog;

public interface SystemLogDao extends BaseDao<SystemLog> {

}
