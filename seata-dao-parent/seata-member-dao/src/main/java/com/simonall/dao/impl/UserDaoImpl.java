package com.simonall.dao.impl;

import org.springframework.stereotype.Repository;

import com.simonall.dao.UserDao;
import com.simonall.member.model.User;
import com.simonall.mysql.dao.impl.BaseDaoImpl;

@Repository(value = "userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {
	 

}
