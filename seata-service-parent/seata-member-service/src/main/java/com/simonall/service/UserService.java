package com.simonall.service;

import com.simonall.member.model.User;
import com.simonall.mysql.service.BaseService;

public interface UserService extends BaseService<User> {

}
