package com.simonall.service.impl;

import org.springframework.stereotype.Service;

import com.simonall.member.model.User;
import com.simonall.mysql.service.impl.BaseServiceImpl;
import com.simonall.service.UserService;

@Service(value = "userService")
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {

}
