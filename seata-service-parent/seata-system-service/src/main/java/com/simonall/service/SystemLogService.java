package com.simonall.service;

import com.simonall.mysql.service.BaseService;
import com.simonall.system.model.SystemLog;

public interface SystemLogService extends BaseService<SystemLog> {

}
