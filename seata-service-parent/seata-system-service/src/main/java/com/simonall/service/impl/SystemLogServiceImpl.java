package com.simonall.service.impl;

import org.springframework.stereotype.Service;

import com.simonall.mysql.service.impl.BaseServiceImpl;
import com.simonall.service.SystemLogService;
import com.simonall.system.model.SystemLog;

@Service(value = "systemLogService")
public class SystemLogServiceImpl extends BaseServiceImpl<SystemLog> implements SystemLogService {

}
