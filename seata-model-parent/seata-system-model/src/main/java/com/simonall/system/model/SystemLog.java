package com.simonall.system.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.simonall.mysql.model.BaseEntity;

/**
 * 系统日志
 * 
 * @author simon
 */
@Entity
@DynamicUpdate
@Table(name = "system_log")
public class SystemLog extends BaseEntity {

	private static final long serialVersionUID = -908996249498898499L;

	// 请求地址
	private String requestUrl;

	// 请求IP
	private String ip;

	@Column(name = "request_url", nullable = false)
	public String getRequestUrl() {
		return requestUrl;
	}

	public void setRequestUrl(String requestUrl) {
		this.requestUrl = requestUrl;
	}

	@Column(name = "ip", nullable = false, length = 128)
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String toString() {
		return "SystemLog [requestUrl=" + requestUrl + ", ip=" + ip + "]";
	}

}
