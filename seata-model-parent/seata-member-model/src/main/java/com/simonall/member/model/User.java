package com.simonall.member.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.simonall.mysql.model.BaseEntity;

/**
 * 用户
 * 
 * @author simon
 */
@Entity
@DynamicUpdate
@Table(name = "member_user")
public class User extends BaseEntity {

	private static final long serialVersionUID = -8026944276671333316L;

	// 用户名
	private String userName;

	// 手机号码
	private String mobile;

	// 邮箱
	private String email;

	// 昵称
	private String nickName;

	// 登录密码
	private String password;

	@Column(name = "user_name", nullable = false, unique = true)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "mobile", unique = true, length = 11)
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "email", unique = true, length = 64)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "nick_name", nullable = false, unique = true, length = 32)
	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Column(name = "password", nullable = true, length = 32)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
