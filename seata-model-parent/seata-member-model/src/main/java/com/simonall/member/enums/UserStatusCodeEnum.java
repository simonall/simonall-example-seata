package com.simonall.member.enums;

/**
 * 用户状态
 * 
 * @author simonall
 */
public enum UserStatusCodeEnum {
	/**
	 * 正常
	 * 
	 */
	ENABLED("正常"),

	/**
	 * 已销户
	 * 
	 */
	CANCELLATION("已销户"),

	/**
	 * 已挂失
	 * 
	 */
	REPORT_THE_LOSS("已挂失"),

	/**
	 * 冻结中
	 * 
	 */
	DISABLED("冻结中");

	private String code;

	private String message;

	private UserStatusCodeEnum(String message) {
		this.code = this.name();
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
